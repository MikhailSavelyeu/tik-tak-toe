﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class BotPlayer : Player
    {
        public override void Turn(GameBoard gameBoard)
        {
            Random rnd = new Random();
            int coordinate;
            Point point = new Point();
            while (true)
            {
                coordinate=rnd.Next(0, gameBoard.GetSize() * gameBoard.GetSize() - 1);
                point.SetX(coordinate / gameBoard.GetSize());
                point.SetY(coordinate % gameBoard.GetSize());
                if (gameBoard.IsEmptyPoints(point))
                {
                    gameBoard.SetCharacter(XorO, point);
                    break;
                }
            }
        }
    }
}
