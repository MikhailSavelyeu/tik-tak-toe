﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class Game
    {
        private Player[] players = new Player[2];
        private GameBoard gameBoard;
        private int first_turn;
        public Game(GameBoard gameBoard, Settings settings)
        {
            
            Random rnd = new Random();
            this.gameBoard = gameBoard;
            players[0] = new Player();
            if (settings.GetPlayerMode())
            {
                players[1] = new Player();
            }
            else
            {
                players[1] = new BotPlayer();
            }
            players[0].SetCharacter(settings.GetCharTurn());
            players[1].SetCharacter(!settings.GetCharTurn());
            switch (settings.GetFirstTurn())
            {
                case -1:
                    {
                        first_turn = 0;
                        break;
                    }
                case 0:
                    {
                        first_turn = rnd.Next(0,1);
                        break;
                    }
                case 1:
                    {
                        first_turn = 1;
                        break;
                    }
            }

        }

        public void Play()
        {
            for (int i = 0; i < gameBoard.GetSize()* gameBoard.GetSize(); i++)
            {
                gameBoard.ConsoleWriteBoard();
                if (i % 2 == first_turn)
                {
                    players[0].Turn(gameBoard);
                    Console.Clear();
                }
                else
                {
                    players[1].Turn(gameBoard);
                    Console.Clear();
                }
            }
            Console.WriteLine(gameBoard.GetSource(players[0].GetXorO()) + ":" +gameBoard.GetSource(players[1].GetXorO()));
            if (gameBoard.GetSource(players[0].GetXorO()) > gameBoard.GetSource(players[1].GetXorO()))
            {
                Console.WriteLine("Победил Игрок1");
            }
            else
            {
                if (gameBoard.GetSource(players[0].GetXorO()) < gameBoard.GetSource(players[1].GetXorO()))
                {
                    Console.WriteLine("Победил Игрок2");
                }
                else 
                {
                    Console.WriteLine("Ничья!");
                }
            }

        }
    }
}
