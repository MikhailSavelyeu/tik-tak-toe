﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class GameBoard
    {
        private char[][] gameboard;
        private int n;
        public GameBoard(int n)
        {
            this.n = n;
            gameboard = new char[n][];
            for (int i = 0; i < n; i++)
            {
                gameboard[i] = new char[n];
            }
            Array.Resize(ref gameboard, n);
            for (int i = 0; i < n; i++)
            {
                Array.Resize(ref gameboard[i], n);
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    gameboard[i][j] = '*';
                }
            }
        }

        public void ConsoleWriteBoard()
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(gameboard[i][..]);
            }
        }

        public int GetSource(char XorO)
        {
            int source=0;
            // Подсчёт по горизонтали
            int сharactersInaRow = 0;
            for (int i = 0; i < gameboard.Length; i++)
            {
                for(int j = 0; j < gameboard.Length; j++)
                {
                    if (gameboard[i][j] == XorO)
                    {
                        сharactersInaRow++;
                        if (сharactersInaRow == 3)
                        {
                            source++;
                            сharactersInaRow = 0;
                        }
                    }
                    else
                    {
                        сharactersInaRow = 0;
                    }
                }
                сharactersInaRow = 0;
            }

            // Подсчёт по вертикали
            сharactersInaRow = 0;
            for (int i = 0; i < gameboard.Length; i++)
            {
                for (int j = 0; j < gameboard.Length; j++)
                {
                    if (gameboard[j][i] == XorO)
                    {
                        сharactersInaRow++;
                        if (сharactersInaRow == 3)
                        {
                            source++;
                            сharactersInaRow = 0;
                        }
                    }
                    else
                    {
                        сharactersInaRow = 0;
                    }
                }
                сharactersInaRow = 0;
            }



            // Подсчёт по побочной диагонали 
            сharactersInaRow = 0;
            for (int j = 2; j < gameboard.Length; j++)
            {
                for (int i = 0; i < gameboard.Length && j - i >= 0; i++)
                {
                    if (gameboard[i][j - i] == XorO)
                    {
                        сharactersInaRow++;
                        if (сharactersInaRow == 3)
                        {
                            source++;
                            сharactersInaRow = 0;
                        }
                    }
                    else
                    {
                        сharactersInaRow = 0;
                    }
                }
                сharactersInaRow = 0;
            }

            сharactersInaRow = 0;
            for (int start_i = gameboard.Length-3; start_i >= 1; start_i--)
            {
                for (int i = 0; i < gameboard.Length && start_i + i < gameboard.Length; i++)
                {
                    if (gameboard[start_i+i][gameboard.Length - i - 1] == XorO)
                    {
                        сharactersInaRow++;
                        if (сharactersInaRow == 3)
                        {
                            source++;
                            сharactersInaRow = 0;
                        }
                    }
                    else
                    {
                        сharactersInaRow = 0;
                    }
                }
                сharactersInaRow = 0;
            }

            // Подсчёт по главной диагонали
            сharactersInaRow = 0;
            for (int start_i = gameboard.Length - 3; start_i >= 1; start_i--)
            {
                for (int j = 0; j < gameboard.Length && start_i + j < gameboard.Length; j++)
                {
                    if (gameboard[start_i + j][j] == XorO)
                    {
                        сharactersInaRow++;
                        if (сharactersInaRow == 3)
                        {
                            source++;
                            сharactersInaRow = 0;
                        }
                    }
                    else
                    {
                        сharactersInaRow = 0;
                    }
                }
                сharactersInaRow = 0;
            }

            сharactersInaRow = 0;
            for (int j = 0; j < gameboard.Length-2; j++)
            {
                for (int i = 0; i < gameboard.Length && j + i < gameboard.Length; i++)
                {
                    if (gameboard[i][j+i] == XorO)
                    {
                        сharactersInaRow++;
                        if (сharactersInaRow == 3)
                        {
                            source++;
                            сharactersInaRow = 0;
                        }
                    }
                    else
                    {
                        сharactersInaRow = 0;
                    }
                }
                сharactersInaRow = 0;

            }

            return source;
        }

        public int GetSize()
        {
            return n;
        }

        public void SetCharacter(char XorO, Point point)
        {
            point.GetCoordinate(out int x,out int y);
            gameboard[x][y] = XorO;
        }

        public bool IsEmptyPoints(Point point)
        {
            point.GetCoordinate(out int x, out int y);
            return gameboard[x][y] == '*';
        }
    }
}
