﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class Menu
    {
        private Settings settings = new Settings();
        public Menu()
        {
            main_menu();
        }
        private void main_menu()
        {
            Console.WriteLine("1. Начать игру");
            Console.WriteLine("2. Настроить режим игры");
            Console.WriteLine("3. Выход из программы");
            bool exit = false;
            while (!exit)
            {
                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        {
                            Console.WriteLine();
                            Console.Clear();
                            Play();
                            Console.WriteLine("Нажмите любую клавишу, чтобы вернутся в главное меню.");
                            Console.ReadKey();
                            Console.Clear();
                            Console.WriteLine("1. Начать игру");
                            Console.WriteLine("2. Настроить режим игры");
                            Console.WriteLine("3. Выход из программы");
                            break;
                        }
                    case '2':
                        {
                            Console.WriteLine();
                            Console.Clear();
                            Settings();
                            Console.WriteLine("1. Начать игру");
                            Console.WriteLine("2. Настроить режим игры");
                            Console.WriteLine("3. Выход из программы");
                            break;
                        }
                    case '3':
                        {
                            exit = true;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("\nНеверный формат ввода! Повторите попытку!");
                            break;
                        }
                }
            }
        }

        private void Play()
        {
            GameBoard gameBoard = new GameBoard(settings.GetSizeGameboard());
            Game game = new Game(gameBoard, settings);
            game.Play();
        }

        private void Settings()
        {
            bool exit = false;
            ShowSettingText();
            while (!exit)
            {
                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        {
                            settings.SetPlayerMode(!settings.GetPlayerMode());
                            Console.Clear();
                            ShowSettingText();
                            break;
                        }
                    case '2':
                        {
                            settings.SetFirstTurn(settings.GetFirstTurn() + 1 == 2 ? -1 : settings.GetFirstTurn() + 1);
                            Console.Clear();
                            ShowSettingText();
                            break;
                        }
                    case '3':
                        {
                            settings.SetCharTurn(!settings.GetCharTurn());
                            Console.Clear();
                            ShowSettingText();
                            break;
                        }
                    case '4':
                        {
                            SetSizeGameboard();
                            Console.Clear();
                            ShowSettingText();
                            break;
                        }
                    case '5':
                        {
                            Console.Clear();
                            return;
                        }
                    default:
                        {
                            Console.WriteLine("\nДанного номера нету в меню! Повторите попытку!");
                            break;
                        }

                }
            }
        }

        private void ShowSettingText()
        {
            Console.WriteLine("1. Игрок против игрока: " + (settings.GetPlayerMode() ? "Да" : "Нет"));
            switch (settings.GetFirstTurn())
            {
                case -1:
                    {
                        Console.WriteLine("2. Первым ходит Игрок1.");
                        break;
                    }
                case 0:
                    {
                        Console.WriteLine("2. Первым ходит случайный игрок.");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("2. Первым ходит Игрок2.");
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            Console.WriteLine("3. Игрок1 ходит : " + (settings.GetCharTurn() ? "X" : "O"));
            Console.WriteLine($"4. Размер поля {settings.GetSizeGameboard()}x{settings.GetSizeGameboard()}");
            Console.WriteLine("5. Выход из настроек.");
        }

        private void SetSizeGameboard()
        {
            Console.WriteLine("Введите размер игрового поля при условии n>=3 и является не чётным:");
            int size;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out size))
                {
                    if (size % 2 == 1 && size>=3)
                    {
                        settings.SetSizeGameboard(size);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Число является чётным или меньше 3! Повторите попытку!");
                    }
                }
                else
                {
                    Console.WriteLine("Неверный формат ввода ! Повторите попытку !");
                }
            }
        }
    }




}

