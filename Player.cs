﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class Player
    {
        protected char XorO;

        public virtual void Turn(GameBoard gameBoard)
        {
            int x=0;
            int y=0;
            Point point = new Point(x, y);
            while (true)
            {
                CreateCoordinate(out x, out y, gameBoard.GetSize());
                point.SetX(x);
                point.SetY(y);
                if (gameBoard.IsEmptyPoints(point))
                {
                    gameBoard.SetCharacter(XorO, point);
                    break;
                }
                else
                {
                    Console.WriteLine("Данная точка уже занята! Выберите другую точку!");
                }
            }
        }

        public void SetCharacter(bool XorO)
        {
            if (XorO)
            {
                this.XorO = 'X';
            }
            else
            {
                this.XorO = 'O';
            }
        }

        private void CreateCoordinate(out int x, out int y,int sizeboard)
        {
            while (true)
            {
                Console.WriteLine("Введите X:");
                if (int.TryParse(Console.ReadLine(), out x))
                {
                    if (x > sizeboard - 1 || x < 0)
                    {
                        Console.WriteLine("Введены не корректные координаты ! Введите корректные координаты!");
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Неверный формат ввода! Повторите попытку!");
                }

            }
            while (true)
            {
                Console.WriteLine("Введите Y:");
                if (int.TryParse(Console.ReadLine(), out y))
                {
                    if (y > sizeboard - 1 || y < 0)
                    {
                        Console.WriteLine("Введены не корректные координаты ! Введите корректные координаты!");
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Неверный формат ввода! Повторите попытку!");
                }

            }
        }

        public char GetXorO()
        {
            return XorO;
        }
    }
}
