﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class Point
    {
        private int x, y;
        public Point()
        {
            x = 0;
            y = 0;
        }

        public Point(int x,int y)
        {
            this.x = x;
            this.y = y;
        }

        public void GetCoordinate(out int x, out int y)
        {
            x = this.x;
            y = this.y;
        }

        public void SetX(int x)
        {
            this.x = x;
        }

        public void SetY(int y)
        {
            this.y = y;
        }
    }
}
