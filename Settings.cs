﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_tac_toe
{
    class Settings
    {
        private bool player_vs_player;
        private int first_turn;
        private bool whoX;
        private int size_gameboard;
        public Settings()
        {
            this.player_vs_player = false;
            this.first_turn = -1;
            this.whoX = true;
            this.size_gameboard = 3;
        }
        public void SetPlayerMode(bool player_vs_player)
        {
            this.player_vs_player = player_vs_player;
        }

        public void SetFirstTurn(int first_turn)
        {
            this.first_turn = first_turn;
        }

        public void SetSizeGameboard(int size_gameboard)
        {
            this.size_gameboard = size_gameboard;
        }

        public void SetCharTurn(bool whoX)
        {
            this.whoX = whoX;
        }

        public bool GetPlayerMode()
        {
            return player_vs_player;
        }

        public int GetFirstTurn()
        {
            return first_turn;
        }

        public int GetSizeGameboard()
        {
            return size_gameboard;
        }

        public bool GetCharTurn()
        {
            return whoX;
        }
    }
}
